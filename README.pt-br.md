PDO Library
===========


As funções mysql_*, pg_* e similares estão obsoletas e nas próximas versões do PHP não mais funcionarão.

Isso nos obriga a procurar alternativas para as mesmas.

A alternativa mais viável entre as existentes e mantidas pela equipe do PHP é o PDO, que suporta vários SGBDs.

Fiz uma pesquisa reunindo várias funções que me atendem substituindo as funções obsoletas e criei outras partindo dos metadados dos SGBDs MySQL e PostgreSQL e criei uma classe para usar em meus aplicativos e sites.
O resultado com alguma documentação está aqui no GitHub.

