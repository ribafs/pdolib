PDO Library
===========

The mysql_*, pg_* and similar are obsoletes and in future versions of PHP will no longer work.

This requires us to seek alternatives for them.

The most viable alternative between existing and maintained by the team of PHP's is PDO that supports multiple DBMSs.

I did some research bringing together several functions that I attend functions replacing obsolete and created other starting from the metadata DBMS MySQL and PostgreSQL and created a class to use in my applications and websites.

The result is here with some documentation on GitHub.
