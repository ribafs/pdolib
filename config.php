<?php
// To improve the safety, copy this file out of the Apache public_html and here only a include_only()
class Config{
	const SGBD = 'mysql';	// mysql - MySQL or pg - PostgreSQL
	const HOST = 'localhost';
	const USER = 'root';
	const PASS = '';
	const DB = 'condominio'; // exemplos em português e english: cadastro / register
	const PORT = '3306';
	const LANG = 'br';	// br - Português do Brasil or en - English
	const RPP = 10;
	const LINKS = 10;
	const TITLE = 'Condomínio Ferreira';
}
?>
